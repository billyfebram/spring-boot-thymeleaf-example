package com.atibusinessgroup.vtf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Test1Controller {
    @GetMapping("/test1")
    public String about(Model model){
        model.addAttribute("name", "Andreas Billy Sutandi");
        return "test1";
    }
}
