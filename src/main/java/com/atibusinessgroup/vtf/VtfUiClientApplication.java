package com.atibusinessgroup.vtf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtfUiClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(VtfUiClientApplication.class, args);
	}

}
